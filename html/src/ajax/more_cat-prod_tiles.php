<?php  
  header("Content-type: text/html, charset=utf-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>


<div class="catalog__card just-loaded">
    <div class="prod-card for-catalog">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_03.jpg" alt="Daikin FTXJ-M/RXJ-M">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Daikin FTXJ-M/RXJ-M</div>
            <div class="prod-card__price">От 95 500 <i class="icon-rub">a</i></div>
        </div>
        <div class="prod-card__more">
            <div class="prod-card__status">
                <div class="prod-card__comment">7 Отзывов</div>
                <div class="prod-card__rate prod-card__rate_four"></div>
            </div>
            <ul class="features-line prod-card__icons">
                <li class="features-line__item"><i class="icon-prod-auto"></i></li>
                <li class="features-line__item"><i class="icon-prod-water"></i></li>
                <li class="features-line__item"><i class="icon-prod-flash"></i></li>
                <li class="features-line__item"><i class="icon-prod-freez"></i></li>
                <li class="features-line__item"><i class="icon-prod-termo"></i></li>
                <li class="features-line__item"><i class="icon-prod-touch"></i></li>
            </ul>
            <a class="prod-card__btn btn btn_block" href="#">Где купить</a>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Daikin FTXJ-M/RXJ-M"></a>
    </div><!-- END prod-card -->
</div><!-- END catalog__card -->

<div class="catalog__card just-loaded">
    <div class="prod-card for-catalog">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_03.jpg" alt="Daikin FTXJ-M/RXJ-M">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Daikin FTXJ-M/RXJ-M</div>
            <div class="prod-card__price">От 95 500 <i class="icon-rub">a</i></div>
        </div>
        <div class="prod-card__more">
            <div class="prod-card__status">
                <div class="prod-card__comment">7 Отзывов</div>
                <div class="prod-card__rate prod-card__rate_four"></div>
            </div>
            <ul class="features-line prod-card__icons">
                <li class="features-line__item"><i class="icon-prod-auto"></i></li>
                <li class="features-line__item"><i class="icon-prod-water"></i></li>
                <li class="features-line__item"><i class="icon-prod-flash"></i></li>
                <li class="features-line__item"><i class="icon-prod-freez"></i></li>
                <li class="features-line__item"><i class="icon-prod-termo"></i></li>
                <li class="features-line__item"><i class="icon-prod-touch"></i></li>
            </ul>
            <a class="prod-card__btn btn btn_block" href="#">Где купить</a>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Daikin FTXJ-M/RXJ-M"></a>
    </div><!-- END prod-card -->
</div><!-- END catalog__card -->

<div class="catalog__card just-loaded">
    <div class="prod-card for-catalog">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_03.jpg" alt="Daikin FTXJ-M/RXJ-M">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Daikin FTXJ-M/RXJ-M</div>
            <div class="prod-card__price">От 95 500 <i class="icon-rub">a</i></div>
        </div>
        <div class="prod-card__more">
            <div class="prod-card__status">
                <div class="prod-card__comment">7 Отзывов</div>
                <div class="prod-card__rate prod-card__rate_four"></div>
            </div>
            <ul class="features-line prod-card__icons">
                <li class="features-line__item"><i class="icon-prod-auto"></i></li>
                <li class="features-line__item"><i class="icon-prod-water"></i></li>
                <li class="features-line__item"><i class="icon-prod-flash"></i></li>
                <li class="features-line__item"><i class="icon-prod-freez"></i></li>
                <li class="features-line__item"><i class="icon-prod-termo"></i></li>
                <li class="features-line__item"><i class="icon-prod-touch"></i></li>
            </ul>
            <a class="prod-card__btn btn btn_block" href="#">Где купить</a>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Daikin FTXJ-M/RXJ-M"></a>
    </div><!-- END prod-card -->
</div><!-- END catalog__card -->