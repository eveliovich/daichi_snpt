<?php  
  header("Content-type: text/html, charset=utf-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>


<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://LKG.ru" target="_blank">LKG.ru</a></span>
            <span class="offer__shop-review">2 664 Отзыва</span>
            <span class="offer__shop-rate rate rate_five"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Авторизованный дилер Daikin.</li>
            <li class="offer__terms-item">Предоплата.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="offer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://daikin-shop.ru" target="_blank">daikin-shop.ru</a></span>
            <span class="offer__shop-review">82 Отзыва</span>
            <span class="offer__shop-rate rate rate_four"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Официальный магазин Daikin.</li>
            <li class="offer__terms-item">Монтаж. Обслуживание.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, до 2 дней.</li>
            <li class="offer__sales-item">Самовывоз.</li>
            <li class="offer__sales-item">Доставка и монтаж в один день.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span><em>-20%</em>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-old"><span>118 750</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://daikinworld.ru" target="_blank">daikinworld.ru</a></span>
            <span class="offer__shop-review">73 Отзыва</span>
            <span class="offer__shop-rate rate rate_four"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Официальный партнер Daikin.</li>
            <li class="offer__terms-item">Монтаж. Проектирование.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, от 4 до 6 дней.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://LKG.ru" target="_blank">LKG.ru</a></span>
            <span class="offer__shop-review">2 664 Отзыва</span>
            <span class="offer__shop-rate rate rate_five"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Авторизованный дилер Daikin.</li>
            <li class="offer__terms-item">Предоплата.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="offer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://daikin-shop.ru" target="_blank">daikin-shop.ru</a></span>
            <span class="offer__shop-review">82 Отзыва</span>
            <span class="offer__shop-rate rate rate_four"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Официальный магазин Daikin.</li>
            <li class="offer__terms-item">Монтаж. Обслуживание.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, до 2 дней.</li>
            <li class="offer__sales-item">Самовывоз.</li>
            <li class="offer__sales-item">Доставка и монтаж в один день.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span><em>-20%</em>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-old"><span>118 750</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://daikinworld.ru" target="_blank">daikinworld.ru</a></span>
            <span class="offer__shop-review">73 Отзыва</span>
            <span class="offer__shop-rate rate rate_four"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Официальный партнер Daikin.</li>
            <li class="offer__terms-item">Монтаж. Проектирование.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, от 4 до 6 дней.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://LKG.ru" target="_blank">LKG.ru</a></span>
            <span class="offer__shop-review">2 664 Отзыва</span>
            <span class="offer__shop-rate rate rate_five"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Авторизованный дилер Daikin.</li>
            <li class="offer__terms-item">Предоплата.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="offer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://daikin-shop.ru" target="_blank">daikin-shop.ru</a></span>
            <span class="offer__shop-review">82 Отзыва</span>
            <span class="offer__shop-rate rate rate_four"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Официальный магазин Daikin.</li>
            <li class="offer__terms-item">Монтаж. Обслуживание.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, до 2 дней.</li>
            <li class="offer__sales-item">Самовывоз.</li>
            <li class="offer__sales-item">Доставка и монтаж в один день.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span><em>-20%</em>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-old"><span>118 750</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://daikinworld.ru" target="_blank">daikinworld.ru</a></span>
            <span class="offer__shop-review">73 Отзыва</span>
            <span class="offer__shop-rate rate rate_four"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Официальный партнер Daikin.</li>
            <li class="offer__terms-item">Монтаж. Проектирование.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, от 4 до 6 дней.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->

<div class="offer prod-tab__offer just-loaded">
    <div class="offer__info">
        <div class="offer__shop">
            <span class="offer__shop-name"><a href="http://LKG.ru" target="_blank">LKG.ru</a></span>
            <span class="offer__shop-review">2 664 Отзыва</span>
            <span class="offer__shop-rate rate rate_five"></span>
        </div>
        <ul class="offer__terms">
            <li class="offer__terms-item">Авторизованный дилер Daikin.</li>
            <li class="offer__terms-item">Предоплата.</li>
            <li class="offer__terms-item">Гарантия производителя.</li>
        </ul>
    </div>
    <div class="offer__sales">
        <div class="offer__sales-title">Акции и спецпредложения:</div>
        <ul class="offer__sales-list">
            <li class="offer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="offer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="offer__price">
        <div class="offer__price-now"><span>95 000</span> <i class="icon-rub">a</i></div>
        <div class="offer__price-btn">
            <a class="btn" href="#" target="_blank">В магазин</a>
        </div>
    </div>
</div><!-- END offer -->