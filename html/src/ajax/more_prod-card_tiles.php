<?php  
  header("Content-type: text/html, charset=utf-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>


<div class="sec-products__card just-loaded">
    <div class="prod-card">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_02.jpg" alt="Midea Blanc MSMA1">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__rate prod-card__rate_four"></div>
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Midea Blanc MSMA1</div>
            <div class="prod-card__price">От 46 800 <i class="icon-rub">a</i></div>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Midea Blanc MSMA1"></a>
    </div><!-- END prod-card -->
</div><!-- END sec-products__card -->

<div class="sec-products__card just-loaded">
    <div class="prod-card">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_03.jpg" alt="Daikin FTXJ-M/RXJ-M">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__rate prod-card__rate_five"></div>
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Daikin FTXJ-M/RXJ-M</div>
            <div class="prod-card__price">От 95 500 <i class="icon-rub">a</i></div>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Daikin FTXJ-M/RXJ-M"></a>
    </div><!-- END prod-card -->
</div><!-- END sec-products__card -->

<div class="sec-products__card just-loaded">
    <div class="prod-card">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_04.jpg" alt="Midea Mission MSMB">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__rate prod-card__rate_four"></div>
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Midea Mission MSMB</div>
            <div class="prod-card__price">От 64 200 <i class="icon-rub">a</i></div>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Midea Mission MSMB"></a>
    </div><!-- END prod-card -->
</div><!-- END sec-products__card -->

<div class="sec-products__card just-loaded">
    <div class="prod-card">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_05.jpg" alt="Daikin FTXS-K/RXS-L">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__rate prod-card__rate_four"></div>
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Daikin FTXS-K/RXS-L</div>
            <div class="prod-card__price">От 76 540 <i class="icon-rub">a</i></div>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Daikin FTXS-K/RXS-L"></a>
    </div><!-- END prod-card -->
</div><!-- END sec-products__card -->

<div class="sec-products__card just-loaded">
    <div class="prod-card">
        <ul class="prod-card__top">
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_comp" role="button">i</a>
            </li>
            <li class="prod-card__top-item">
                <a class="prod-card__top-btn prod-card__top-btn_like" role="button">i</a>
            </li>
        </ul>
        <div class="prod-card__pict">
            <img class="prod-card__img" src="img/sec-products/product_03.jpg" alt="Daikin FTXJ-M/RXJ-M">
        </div>
        <div class="prod-card__text">
            <div class="prod-card__rate prod-card__rate_five"></div>
            <div class="prod-card__type">Настенная сплит-система</div>
            <div class="prod-card__name">Daikin FTXJ-M/RXJ-M</div>
            <div class="prod-card__price">От 95 500 <i class="icon-rub">a</i></div>
        </div>
        <a class="prod-card__link" href="#" title="Посмотреть Daikin FTXJ-M/RXJ-M"></a>
    </div><!-- END prod-card -->
</div><!-- END sec-products__card -->
