<?php  
  header("Content-type: text/html, charset=utf-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>


<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.ablgroup.ru" target="_blank">АБЛ-Инженеринг Групп</a></div>
        <div class="dealer__text">
            Адрес: 115088, Москва,<br>
            ул. Шарикоподшипниковская, 13а<br>
            Телефон: (495) 221-74-30
        </div>
        <div class="dealer__site"><a href="http://www.ablgroup.ru" target="_blank">www.ablgroup.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="dealer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_five"></div>
        <div class="dealer__review">2 664 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.ablgroup.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.avks.ru" target="_blank">АВК cтрой-маркет</a></div>
        <div class="dealer__text">
            Адрес: Москва, 16-я Парковая ул., д. 26,<br>
            корп. 20<br>
            Телефон: +7 (495) 987-44-03<br>
            Факс: (495) 987-44-03
        </div>
        <div class="dealer__site"><a href="http://www.avks.ru" target="_blank">www.avks.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, до 2 дней.</li>
            <li class="dealer__sales-item">Самовывоз.</li>
            <li class="dealer__sales-item">Доставка и монтаж в один день.</li>
        </ul>
        <ul class="dealer__features">
            <li class="dealer__features-item">Золотой партнёр</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_four"></div>
        <div class="dealer__review">82 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.avks.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.avk-klimat.ru" target="_blank">АВК-Климат</a></div>
        <div class="dealer__text">
            Адрес: Москва, пр-д Серебрякова, д. 14 корп. 6,<br>
            оф. 6204.<br>
            Метро: Свиблово<br>
            Телефон: (495) 923-82-55
        </div>
        <div class="dealer__site"><a href="http://www.avk-klimat.ru" target="_blank">www.avk-klimat.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, от 4 до 6 дней.</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_four"></div>
        <div class="dealer__review">73 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.avk-klimat.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.ablgroup.ru" target="_blank">АБЛ-Инженеринг Групп</a></div>
        <div class="dealer__text">
            Адрес: 115088, Москва,<br>
            ул. Шарикоподшипниковская, 13а<br>
            Телефон: (495) 221-74-30
        </div>
        <div class="dealer__site"><a href="http://www.ablgroup.ru" target="_blank">www.ablgroup.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="dealer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_five"></div>
        <div class="dealer__review">2 664 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.ablgroup.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.avks.ru" target="_blank">АВК cтрой-маркет</a></div>
        <div class="dealer__text">
            Адрес: Москва, 16-я Парковая ул., д. 26,<br>
            корп. 20<br>
            Телефон: +7 (495) 987-44-03<br>
            Факс: (495) 987-44-03
        </div>
        <div class="dealer__site"><a href="http://www.avks.ru" target="_blank">www.avks.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, до 2 дней.</li>
            <li class="dealer__sales-item">Самовывоз.</li>
            <li class="dealer__sales-item">Доставка и монтаж в один день.</li>
        </ul>
        <ul class="dealer__features">
            <li class="dealer__features-item">Золотой партнёр</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_four"></div>
        <div class="dealer__review">82 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.avks.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.avk-klimat.ru" target="_blank">АВК-Климат</a></div>
        <div class="dealer__text">
            Адрес: Москва, пр-д Серебрякова, д. 14 корп. 6,<br>
            оф. 6204.<br>
            Метро: Свиблово<br>
            Телефон: (495) 923-82-55
        </div>
        <div class="dealer__site"><a href="http://www.avk-klimat.ru" target="_blank">www.avk-klimat.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, от 4 до 6 дней.</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_four"></div>
        <div class="dealer__review">73 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.avk-klimat.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.ablgroup.ru" target="_blank">АБЛ-Инженеринг Групп</a></div>
        <div class="dealer__text">
            Адрес: 115088, Москва,<br>
            ул. Шарикоподшипниковская, 13а<br>
            Телефон: (495) 221-74-30
        </div>
        <div class="dealer__site"><a href="http://www.ablgroup.ru" target="_blank">www.ablgroup.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="dealer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_five"></div>
        <div class="dealer__review">2 664 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.ablgroup.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.avks.ru" target="_blank">АВК cтрой-маркет</a></div>
        <div class="dealer__text">
            Адрес: Москва, 16-я Парковая ул., д. 26,<br>
            корп. 20<br>
            Телефон: +7 (495) 987-44-03<br>
            Факс: (495) 987-44-03
        </div>
        <div class="dealer__site"><a href="http://www.avks.ru" target="_blank">www.avks.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, до 2 дней.</li>
            <li class="dealer__sales-item">Самовывоз.</li>
            <li class="dealer__sales-item">Доставка и монтаж в один день.</li>
        </ul>
        <ul class="dealer__features">
            <li class="dealer__features-item">Золотой партнёр</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_four"></div>
        <div class="dealer__review">82 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.avks.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.avk-klimat.ru" target="_blank">АВК-Климат</a></div>
        <div class="dealer__text">
            Адрес: Москва, пр-д Серебрякова, д. 14 корп. 6,<br>
            оф. 6204.<br>
            Метро: Свиблово<br>
            Телефон: (495) 923-82-55
        </div>
        <div class="dealer__site"><a href="http://www.avk-klimat.ru" target="_blank">www.avk-klimat.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, от 4 до 6 дней.</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_four"></div>
        <div class="dealer__review">73 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.avk-klimat.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->

<div class="dealer prod-tab__offer just-loaded">
    <div class="dealer__info">
        <div class="dealer__name"><a href="http://www.ablgroup.ru" target="_blank">АБЛ-Инженеринг Групп</a></div>
        <div class="dealer__text">
            Адрес: 115088, Москва,<br>
            ул. Шарикоподшипниковская, 13а<br>
            Телефон: (495) 221-74-30
        </div>
        <div class="dealer__site"><a href="http://www.ablgroup.ru" target="_blank">www.ablgroup.ru</a></div>
    </div>
    <div class="dealer__sales">
        <div class="dealer__sales-title">Акции и спецпредложения:</div>
        <ul class="dealer__sales-list">
            <li class="dealer__sales-item">Бесплатная доставка, от 2 до 6 дней.</li>
            <li class="dealer__sales-item">Монтаж в подарок.</li>
        </ul>
    </div>
    <div class="dealer__more">
        <div class="dealer__rate rate rate_five"></div>
        <div class="dealer__review">2 664 Отзыва</div>
        <div class="dealer__more-btn">
            <a class="btn" href="http://www.ablgroup.ru" target="_blank">Перейти на сайт</a>
        </div>
    </div>
</div><!-- END dealer -->