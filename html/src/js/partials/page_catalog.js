$(function () {
	  /*--- Page Catalog: btn mob filter ------------------------------------*/
	  $(".js-mob-filter_toggle").click(function () {
		var $btn = $(this),
		  $catSide = $("#js-catalog-side");

		if ( $btn.hasClass("is-open") ) {
		  $btn.removeClass("is-open");
		  $catSide.removeClass("is-open");
		} else {
		  $btn.addClass("is-open");
		  $catSide.addClass("is-open");
		}
	  });
  
	/*--- Page Catalog: btn mob filter ------------------------------------*/
	$(".js-mob-filter-toggle").click(function () {
		var $btn = $(this),
			$catSide = $("#js-catalog-side");

		if ($btn.hasClass("is-open")) {
			$btn.removeClass("is-open");
			$catSide.removeClass("is-open");
		} else {
			$btn.addClass("is-open");
			$catSide.addClass("is-open");
		}
	});


	/*--- Page Catalog: Sorting ------------------------------------*/
	$(".js-cat-sort").click(function () {
	var $btn = $(this),
		$catSortList = $("#cat-sort-list"),
		$thisSortItem = $btn.parent(".sorting__by-item"),
		$thisBtnIcon =  $btn.children(".sorting__icon"),
		$catSortAllIcons =  $catSortList.find(".sorting__icon"),
		$catSortAllItems = $catSortList.children(".sorting__by-item"),
		$catCards = $("#js-for-load-cat-prod").children(".catalog__card");

	if ( $thisSortItem.hasClass("is-active") ) {
	  if ( $thisBtnIcon.hasClass("icon-az") ) {
		$thisBtnIcon.removeClass("icon-az").addClass("icon-za");
	  } else {
		$thisBtnIcon.removeClass("icon-za").addClass("icon-az");
	  }
	} else {
	  $catSortAllItems.removeClass("is-active");
	  $catSortAllIcons.removeClass("icon-za icon-az");
	  $thisSortItem.addClass("is-active");
	  $thisBtnIcon.addClass("icon-az");
	}

	$catCards.addClass("is-hidden");
	setTimeout(function () {
	  $catCards.removeClass("is-hidden");
	}, 600);

	console.log("sorted!")

	});
});