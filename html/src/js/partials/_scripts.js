/*----------- FUNCTIONS AFTER DOM READY --------------------------------------*/
$(document).ready(function () {

    /*--- FUNCTIONS: id-Tabs ---------------------------------------------*/
    $.fn.tabsInit = function () {
        var $tabsHead = $(this),
            $links = $tabsHead.find(".js-tabs-link"),
            $tabsBody = $tabsHead.siblings(".js-tabs-body"),
            $tabs = $tabsBody.find(".js-tabs-item");
        $links.click(function () {
            var tabId = "#" + $(this).data("tab");
            $links.removeClass("is-active");
            $(this).addClass("is-active");
            $tabs.removeClass("is-active");
            $(tabId).addClass("is-active");
        });
        return this;
    };
    // Инициализация работы табов
    $(".js-tabs-head").tabsInit();
    $(".js-prod-tabs").tabsInit();


    /*--- FUNCTION: Spoiler works -----------------------------------------*/
    $.fn.spoilerInit = function (startHeight) {
        var $spoilerBtns = $(this);
        startHeight = startHeight || 0;

        $spoilerBtns.each(function (index, elem) {
            var $spoilerBtn = $(this),
                $spoilerBody = $spoilerBtn.siblings(".js-spoiler-body"),
                autoHeight = $spoilerBody.css('height', 'auto').height();

            if (autoHeight > startHeight) {
                $spoilerBody.height(startHeight);
            } else {
                $spoilerBtn.hide();
            }

            $spoilerBtn.click(function () {
                if ($spoilerBtn.hasClass("active")) {
                    $spoilerBody.animate({height: startHeight}, 600);
                    $spoilerBtn.removeClass("active").text("Все характеристики");
                } else {
                    $spoilerBody.animate({height: autoHeight}, 600);
                    $spoilerBtn.addClass("active").text("Свернуть");
                }
            });
        });
        return this;
    };
    //  Init with height on Product page
    $(".js-features-spoiler").spoilerInit(165);


    /*--- Spoiler on Product page ----------------------------------------*/
    $(".js-prod-tab-spoiler").click(function () {
        var $btn = $(this),
            $content = $btn.siblings(".js-spoiler-body");

        if ($btn.hasClass("is-active")) {
            $btn.removeClass("is-active");
            $content.slideUp(600);
        } else {
            $btn.addClass("is-active");
            $content.slideDown(600);
            setTimeout(resetSimilarMobSlider, 100);
        }
    });


    /*--- Wide intro slider ----------------------------------------------*/
    $(".js-intro-slider").slick({
        arrows: false,
        dots: true,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        autoplay: false
    });


    /*--- Sec watched slider ----------------------------------------------*/
    $(".js-watched-slider").slick({
        arrows: false,
        dots: false,
        infinite: false,
        speed: 600,
        autoplay: false,
        variableWidth: true,
        slidesToScroll: 1
    });


  /*--- Sec brands slider ----------------------------------------------*/
  $(".js-brands-slider").slick({
    arrows: true,
    dots: false,
    infinite: false,
    speed: 600,
    autoplay: false,
    variableWidth: true,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 960,
        settings: {
          arrows: false
        }
      }
    ]
  });


  /*--- Sec brands links/tabs -------------------------------------------*/
  $(".js-brands-link").click(function () {
    var $link = $(this),
        tabId = $link.data("brand"),
        $tabs = $("#brands-tabs").children(".sec-brands__tab"),
        $links = $(".js-brands-link");

    if ( $link.hasClass("is-active") ) {
      $link.removeClass("is-active").children(".sec-brands__item-txt").text("Показать оборудование");
      $( tabId ).removeClass("is-active");
    } else {
      $links.removeClass("is-active").children(".sec-brands__item-txt").text("Показать оборудование");
      $tabs.removeClass("is-active");
      $link.addClass("is-active").children(".sec-brands__item-txt").text("Скрыть оборудование");
      $( tabId ).addClass("is-active");
    }
  });

    /*--- Top slider on Product page --------------------------------------*/
    $("#prod-thumbs").slick({
        vertical: true,
        arrows: false,
        dots: false,
        infinite: false,
        speed: 600,
        autoplay: false,
        slidesToShow: 3,
        centerMode: true,
        focusOnSelect: true,
        slidesToScroll: 1,
        asNavFor: '#prod-imgs'
    });

    $("#prod-imgs").slick({
        arrows: false,
        dots: false,
        infinite: false,
        speed: 600,
        autoplay: false,
        slidesToScroll: 1,
        asNavFor: '#prod-thumbs',
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    dots: true
                }
            }
        ]
    });


    /* --- Product slider init -----------------------------------------*/
    function runSimilarMobSlider() {
        $(".js-similar-mob-slider").slick({
            arrows: true,
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            autoplay: false,
            autoplaySpeed: 3000,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 559,
                    settings: 'unslick'
                }
            ]
        });
    }

    function resetSimilarMobSlider() {
        if (window.matchMedia('(max-width: 559px)').matches || $(document).width < 559) {
            $(".js-similar-mob-slider").slick("unslick");
            runSimilarMobSlider();
        }
    }

    runSimilarMobSlider();
    // listen to jQuery's window resize

    function structure(){
        $('.structure__row').each(function(){
            $(this).css('min-height',$(this).outerHeight() + 25);
        });
    }

    structure();

    $(window).on('resize', resetSimilarMobSlider);


    /* --- More content loading -------------------------------------------*/
    function dataAjaxLoad(loadingBtn, loadingPlace, addr, counter, forCount) {
        $(loadingBtn).click(function () {
            var $loadingBtn = $(this),
                $loadingPlace = $(loadingPlace);

            if ($loadingPlace.length) {
                $loadingBtn.addClass("loading");
                $.ajax({
                    type: "POST",
                    url: addr,
                    dataType: "html",
                    cache: false,
                    error: function () {
                        console.log("Error loading more");
                        $loadingBtn.removeClass("loading");
                    },
                    success: function (poupHtml) {
                        console.log("Success loading more");
                        $loadingPlace.append(poupHtml);
                        setTimeout(function () {
                            $loadingPlace.addClass("has-added");
                            $loadingPlace.find(".just-loaded").removeClass("just-loaded"); //children заменено на find, т.к. иногда структура двухуровневая
                            $loadingBtn.removeClass("loading");
                        }, 1000);
                    }
                });
            }
        });
    }

    // Load more prod-cards in Product section
    dataAjaxLoad(".js-load-prod-card", "#js-for-load-prod-cards", "ajax/more_prod-card_tiles.php");
    // Load more prod-cards in Catalog page
    dataAjaxLoad(".js-load-cat-prod", "#js-for-load-cat-prod", "ajax/more_cat-prod_tiles.php");
    // Load more offers in Product page
    dataAjaxLoad(".js-load-offers", "#js-for-load-offers", "ajax/more_offers.php");
    // Load more dealers in Product page
    dataAjaxLoad(".js-load-dealers", "#js-for-load-dealers", "ajax/more_dealers.php");
	// Load more news in News page
    dataAjaxLoad(".js-load-news-list", "#js-for-news-list", "ajax/more_news.php");
	// Load more articles in Articles page
    dataAjaxLoad(".js-load-articles-list", "#js-for-articles-list", "ajax/more_articles.php");
	// Load more objects in Object page
    dataAjaxLoad(".js-load-more-objects", "#js-for-more-objects", "ajax/more_objects.php");


  /*--- Page Product: show Reviews tab btn ------------------------------*/
  $(".js-reviews-tab_show").click(function () {
    var $tabsHead = $("#prod-tabs-nav"),
        targetTop;

    $tabsHead.find('[data-tab = prod_reviews]').trigger('click');
    targetTop = $tabsHead.offset().top - 81;
    $("html, body").animate({scrollTop: targetTop}, 600);
  });

  /*--- Page Product: show Video tab btn  -------------------------------*/
  $(".js-video-tab_show").click(function () {
    $("#prod-tabs-nav").find('[data-tab = prod_posting]').trigger('click');
  });

  $('.has-thumbnails, .news__thumb').not('.silver,.orange').each(function(){
    $(this).css('background-image', 'url('+ $(this).find('> img').attr('src') +')');
  });


  /*--- Header: Product menu show ---------------------------------------*/
  var $prodMenu = $("#prod-menu"),
      $prodMenuBody = $("#prod-menu-body"),
      $prodSubMenuLinks = $(".js-prod-submenu-link");

  $(".js-prod-menu_show").click(function () {
    $prodMenu.addClass("is-active");
  });

  $(".js-prod-menu_hide").click(function () {
    $prodMenu.removeClass("is-active");
  });

  $prodSubMenuLinks.click(function () {
    var $menuLink = $(this);

    if ( $menuLink.hasClass("is-active") ) {
      $menuLink.removeClass("is-active");
      $prodMenuBody.removeClass("is-open");
    } else {
      $prodSubMenuLinks.removeClass("is-active");
      $menuLink.addClass("is-active");
      $prodMenuBody.addClass("is-open");
    }
  });


  /*--- Header: Black-to-white ------------------------------------------*/
  var $headerDesktop = $("#header-desktop");
  $(window).scroll(function () {


    if ( $(window).scrollTop() > 10 ) {
      if ( $headerDesktop.hasClass("header-desktop_black") ) {
        $headerDesktop.removeClass("header-desktop_black").addClass("header-desktop_white was-black");
      }
	  if ( $headerDesktop.hasClass("header-desktop_silver") ) {
        $headerDesktop.removeClass("header-desktop_silver").addClass("header-desktop_white was-silver");
      }
    } else {
      if ( $headerDesktop.hasClass("was-black") ) {
        $headerDesktop.addClass("header-desktop_black").removeClass("header-desktop_white was-black");
      }
	  if ( $headerDesktop.hasClass("was-silver") ) {
        $headerDesktop.addClass("header-desktop_silver").removeClass("header-desktop_white was-silver");
      }
    }


  });


    $('.gallery__slider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        var i = (currentSlide ? currentSlide : 0) + 1;
        $('.gallery__slider-info').text(i + ' / ' + slick.slideCount);
    });
    /*--- Simple accordion ------------------------------------------------*/
    $(".js-accordion-head").click(function () {
        var $acHead = $(this),
            $acBody = $acHead.siblings(".js-accordion-body");

        if ($acHead.hasClass("is-open")) {
            $acHead.removeClass("is-open");
            $acBody.slideUp(300);
        } else {
            $acHead.addClass("is-open");
            $acBody.slideDown(300);
        }
    });

  /*--- Page Product: show Video tab btn  -------------------------------*/
    $(".js-show-video-tab").click(function () {
        $("#prod-tabs-nav").find('[data-tab = prod_posting]').trigger('click');
    });

    $('.news .has-thumbnails').each(function () {
        $(this).css('background-image', 'url(' + $(this).find('.news__bg').attr('src') + ')');
    });

    $('.gallery__slider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
          var i = (currentSlide ? currentSlide : 0) + 1;
          $('.gallery__slider-info').text(i + ' / ' + slick.slideCount);
      });



      $('.gallery__slider').slick({
          infinite: true,
          touchMove: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows: true,
          variableWidth: true
      });

      $('.news-page__slider,.object-page__slider').slick({
          infinite: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          arrows: false
      });      

	$('.showmore').click(function(e){
		e.preventDefault();
		var text = $(this).find('.text'),
			container = $(this).closest('.partners__row, .structure__row');

		$(this).toggleClass('js-active active');

		if ($(this).hasClass('active')){
			container.addClass('js-active active');
			text.text(text.data('open'))
		}
		else{
			container.removeClass('js-active active');
			text.text(text.data('close'))
		}
	});

  $('.switcher-list a').click(function(e){
    e.preventDefault();
    $('.switcher-list li').removeClass('active');
    $(this).parent().addClass('active');
  });

    // Инициализация работы анимации
    new WOW().init();
    /*--- Promo Page: Слайдер моделей   ------------------------------- */
    $('.promo-traits__slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        nextArrow: '<button class="btn btn_clean slider-arrow slider-arrow-next"><svg class="icon icon-slider-arrow"><use xlink:href="#arrow-right"></use></svg></button>',
        prevArrow: '<button class="btn btn_clean slider-arrow slider-arrow-prev"><svg class="icon icon-slider-arrow"><use xlink:href="#arrow-left"></use></svg></button>'
    });

  // Плавный скролл
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          event.preventDefault();
		  var offset = $('#header-desktop:visible').length?$('#header-desktop').outerHeight():0;
          $('html, body').animate({
            scrollTop: target.offset().top - offset
          }, 1000, function() {
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) {
              return false;
            } else {
              $target.attr('tabindex','-1');
              $target.focus();
            };
          });
        }
      }
    });
});