// Пульт Axioma
function pultParallax() {
    var pult = $("#pult")
	if ($("#pult").length)
		$(window).scroll(function () {
			var finish = pult.offset().top - $(window).height() + pult.outerHeight() / 2;
			var start = pult.offset().top + pult.outerHeight() / 2;
			var P = (pult.height() / 3) * ($(document).scrollTop() - start) / (finish - start);

			if ((P < (pult.height() / 5)) && (P > 0)) {
				pult.css({
					'bottom': P + 'px'
				});
			} else if (P < 0) {
				pult.css({
					'bottom': 0 + 'px'
				});
			}

		});
}
$(window).load(pultParallax);

//promo FTXS
$(function () {
	//полоски энергопотребления
	function progress() {
		$('.promo-progress span').each(function(){
			var elem = $(this),
			width = 1,
			maxWidth = elem.data('width'),
			id = setInterval(frame, 1);
			function frame() {
				if (width >= maxWidth) {
					clearInterval(id);
				} else {
					width++;
					elem.css('width', width + '%');
				}
			}
		})
	}

	//проценты энергопотребления
	function numberMove(){
		$('.counter').each(function () {
			$(this).prop('Counter',0).animate({
				Counter: 30
			}, {
				duration: 4000,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	}

	if ($('.promo-ftxs-comfort').length) {
		//анимация начинается, когда на нижней трети экрана появляется нижняя треть блока
		var promoThresh = $('.promo-ftxs-comfort').offset().top + $('.promo-ftxs-comfort').outerHeight()/3*2;

		$(window).on('resize', function () {
			promoThresh = $('.promo-ftxs-comfort').offset().top + $('.promo-ftxs-comfort').outerHeight()/3*2;
		});

		$(window).on('scroll.numbers', function () {
			if ($(window).scrollTop() + $(window).height() / 3*2 >= promoThresh) {
				numberMove();
				progress();
				$(window).off('scroll.numbers');
			}
		});

	}

});


$(function () {
	 // Инициализация sticky-float
    var resizeTimer;

    stickyFloatInit();

    $(window).on('resize', function () {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(stickyFloatInit, 100);
    });

    function stickyFloatInit() {
      if ($('.js-img').length) {
        $('.js-img').each(function () {
          var $self = $(this);
          var $currentContent = $self.closest('.js-parent-sticky-float').find('.js-sticky-float');

          if ($self.height() > $(window).height() && $(window).width() >= 1200) {
            $currentContent.stickyfloat({
               // offsetY: parseInt($('.header-desktop').outerHeight())
            });
          } else {
            $currentContent.stickyfloat('destroy');
            $currentContent.attr('style', '');
          }
        });
      }
    }

    // Плавное появление блока удаленное управление из промо Media Kid Star
    var scrollTimerControl;

    $(window).on('scroll', function () {
      if ($('.js-control-content').length) {
        if (scrollTimerControl) {
          clearTimeout(scrollTimerControl);
        }

        scrollTimerControl = setTimeout(function () {
          var $self = $(this);
          var $content = $('.js-control-content');
          var $step = $('.js-control-step');

          // Точка, когда показываем элементы
          if ($(window).scrollTop() >= ($content.offset().top + $content.height() / 2)  - ($(window).height() / 3) * 2 - $('.header-desktop').outerHeight()) {
            $step.each(function() {
              var $self = $(this);

              $self.addClass('fadeInUp').css({
                'transition-delay': $(this).data('transition-delay')
              });

              $self.closest('.js-control-wrap').find('.js-control-line').addClass('fade');
            });
          }
        }, 30);
      }
    });

    // Логика движения одуванчиков на промо Daikin MC070
	var $allergenContent = $('.js-allergen-content');

	if ($allergenContent.length) {

		var $dandelionTop = $('.js-dandelion-top');
		var $dandelionBottom = $('.js-dandelion-bottom');

		if ($dandelionTop.length && $dandelionBottom.length) {
			var contentCenterPos = $allergenContent.offset().top + $allergenContent.outerHeight() / 2; // центр контента - целевое состояние (как в макете)

			var startPosTopDandelion = ($(window).width() - $dandelionTop.offset().left);
			var startPosBottomDandelion = ($(window).width() - $dandelionBottom.offset().left);

			$(window).on('scroll', function() {
			  var windowScrollTop = $(window).scrollTop();

				$dandelionTop.css({
				  'transform': 'translateX(' + (-windowScrollTop + (contentCenterPos - $(window).height() / 2)) + 'px)'
				});

				$dandelionBottom.css({
				  'transform': 'translateX(' + (-windowScrollTop + (contentCenterPos - $(window).height() / 2)) * 0.3 + 'px)' //множитель - для эффекта параллакса
				});

			});
		}

	}
});

//полоски энергопотребления mideaMission
function percentProgress() {

    var elem = $('.energy-block .energy__bar')[0],
        width = 0,
        maxWidth = $(elem).data('width'),
        id = setInterval(frame, 2);

        function frame() {
            if (width >= maxWidth) {
                clearInterval(id);
            } else {
                width++;
                $(elem).css('width', width + '%');

                if (width >= 33 && !$('.energy_eco .energy__bar').hasClass('is-full')) {
                    $('.energy_eco .energy__bar').addClass('is-full');
                }
            }
        }
}

if ($('.energy-block').length > 0) {
    $('.energy-block').appear();

    $('.energy-block').on('appear', function(event, $all_appeared_elements) {
        //percentProgress();
        $('.energy__bar').addClass('is-full');
    });
}

if ($('.promo-traits-inverter').length > 0) {
    $('.promo-traits-inverter').appear();

    $('.promo-traits-inverter').on('appear', function(event, $all_appeared_elements) {
        $('.promo-traits-inverter img').addClass('is-faded');
    });
}
