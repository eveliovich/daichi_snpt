var FastShop = function() {
    var self = this,
        shop = $('.js-shop'),
        equipBtn = $('[data-shop]'),
        listItem = $('[data-item]'),
        closeBtn = $('.mobile-shop .icon-cross'),
        prevBtn = $('.mobile-shop .icon-arrow'),
        menu,
        item,
        timerId;

    self.open = function(nextStep) {
        $(nextStep).addClass('is-visible');
    };

    self.openNext = function(nextItem) {
        $('[data-list="' + nextItem + '"]').addClass('is-visible');
    };

    self.openPrev = function(currentItem) {
        $('[data-list="' + currentItem + '"]').removeClass('is-visible');
    };

    self.close = function() {
        $('.mobile-shop.is-visible').removeClass('is-visible');
    }

    equipBtn.click(function() {
        menu = $(this).data('shop');
        self.open(menu);
    });

    listItem.click(function() {
        submenu = $(this).data('item');
        self.openNext(submenu);
    });

    closeBtn.click(function() {
        self.close();
    });

    prevBtn.click(function() {
        item = $(this).closest('.mobile-shop').data('list');
        self.openPrev(item);
    });

    $(window).resize(function(e) {
        clearTimeout(timerId);

        timerId = setTimeout(function() {
            if ($(window).width() > global.tabletLgSize) {
                self.close();
            }
        }, 250);
    });

};

var fastShop = new FastShop();
