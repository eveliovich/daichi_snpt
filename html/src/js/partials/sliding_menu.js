var SlidingMenu = function() {
    var self = this,
        menu = $('.js-sliding-menu'),
        btnOpen = $('.js-menu-open');
        btnClose = $('.js-menu-close');

    /**
     * Open sliding menu
     */
    self.open = function() {
        menu.addClass('is-visible');

        $('body').addClass('no-scroll');
    };

    /**
     * Close sliding menu
     */
    self.close = function() {
        menu.removeClass('is-visible');

        $('body').removeClass('no-scroll');
    };

    btnOpen.on('click', function() {
        self.open();
    });

    btnClose.on('click', function() {
        self.close();
    });

};

var slidingMenu = new SlidingMenu();



$(".js-buy-menu_show").click(function () {
	$(this).toggleClass("is-active");
	$(this).next(".main-nav-subnav").toggleClass("is-active");
});

$('body').on('mouseleave', '.main-nav-subnav.is-active', function () {
	$(this).removeClass('is-active');
});