$(function () {
    /*--- Page Offices: more click ------------------------------------*/
    $(".offices-row__link").click(function () {

        var $container = $(this).closest(".offices-row__container");
        var $office_row = $container.parent();
        var $more_class = ".offices-row__hidden";
        var $active_class = "offices-row_active";
        var $active_link = "offices-row__link_active";
        var $more = $container.children($more_class);

        if ((!$(this).hasClass($active_link))) {

            if (!$(this).hasClass('offices-row__close')) {
                // Clear
                $('.offices-list div').removeClass($active_class);
                $('.offices-list a').removeClass($active_link);
                $($more_class).hide();
                $("a.offices-row__link").html("больше информации <span class='icon icon-arrow-right_small'></span>");

                $office_row.height($office_row.outerHeight());
                $container.addClass($active_class);
                $(this).addClass($active_link);
                $(this).html("свернуть <span class='icon icon-arrow-right_small'></span>");
                $more.show();
            } else {

                $office_row.height('auto');
                $container.removeClass($active_class);
                $('a.offices-row__link').removeClass($active_link);
                $('a.offices-row__link').html("больше информации <span class='icon icon-arrow-right_small'></span>");
                $more.hide();

            }

        } else {

            $office_row.height('auto');
            $container.removeClass($active_class);
            $(this).removeClass($active_link);
            $(this).html("больше информации <span class='icon icon-arrow-right_small'></span>");
            $more.hide();

        }
    });

    /*--- Page Offices: Переключатель карта/список ------------------------------------*/
    $(".offices-switch__map").click(function () {
        $(".offices__container").hide();
        $(".offices-map").show();
        $(".offices-switch li").removeClass('active');
        $(this).parent().toggleClass('active');
    });

    $(".offices-switch__list").click(function () {
        $(".offices-map").hide();
        $(".offices__container").show();
        $(".offices-switch li").removeClass('active');
        $(this).parent().toggleClass('active');
    });

    /*--- Page Offices: Карта перемещение ------------------------------------*/
    $('.offices-map__external').kinetic();

    /*--- Page Offices: Карта клик ------------------------------------*/
    $(".offices-map__town").on("click touchstart", function (event) {

        var $active_town = "offices-map__town_active";
        var $target_parent = event.target.parentElement.className;
        var $town_pos = $(this).position().top;
        var $desc_block = $(this).children(".offices-row");
        var $desc_margin = 25;
        var WindowWidth = $(window).width();


        if (WindowWidth < global.tabletSize) {
            //console.log($desc_block.html());
            $(".offices__overlay").show();
            $(".offices__popup").html('<div class="offices-row">' + $desc_block.html() + "</div>");
            $(".offices__popup").show();

            $(".offices-row__mapclose, .offices__overlay").on('click touchstart', function () {
                $(".offices__popup").hide();
                $(".offices-map__town").removeClass('offices-map__town_active');
                $(".offices-map__town .offices-row").hide();
                $(".offices__overlay").hide();
            });
        } else {

            if (!$(this).hasClass($active_town)) {

                // Clear
                $(".offices-map__external .offices-row").hide();
                $(".offices-map__town").removeClass($active_town);

                if (($town_pos - $desc_margin) < $desc_block.height()) {
                    $desc_block.prepend('<div class="offices-row__bottom offices-row__bottom_before"></div>');
                    $desc_block.css({top: $desc_block.height() + $(this).height() + $desc_margin});
                } else {
                    $desc_block.append('<div class="offices-row__bottom"></div>');
                }

                if (($target_parent == 'offices-map__inner')) {
                    $(this).addClass($active_town);
                    $desc_block.show();
                }

            } else {

                if (($target_parent == 'offices-map__inner')) {
                    $(this).removeClass($active_town);
                    $desc_block.hide();
                }

            }
        }
    });

    /*--- Page Offices: Карта закрытие ------------------------------------*/
    $(".offices-row__mapclose").on('click touchstart', function () {
        $(this).closest(".offices-map__town").removeClass('offices-map__town_active');
    });


});

