/**
 * UI аккордион
 * @see  http://api.jqueryui.com/accordion/
 */

var $ftrAccordion = $(".js-accordions-footer");

function ftrAccordionInit() {
  $ftrAccordion.accordion({
        header: '.js-accordion-head',
        animate: global.time,
        collapsible: true,
        heightStyle: 'content',
        beforeActivate: function(event, ui) {
            if ($(event.currentTarget).hasClass('is-disabled')) {
                return false;
            }
        }
    });
}

$(window).load(function() {
    ftrAccordionInit();
    toggleFtrAccordion();
});

function toggleFtrAccordion() {
    if ( $ftrAccordion.length > 0 ) {
        if ( $(window).width() <= global.tabletLgSize ) {
            ftrAccordionInit();
        } else {
            if ($('.ui-accordion.js-accordions-footer').length > 0) {
              $ftrAccordion.accordion('destroy');
            }
        }
    }
}

var accordionTimer;
$(window).resize(function(e) {
    accordionTimer = setTimeout(function() {
      toggleFtrAccordion();
    }, 250);
});
