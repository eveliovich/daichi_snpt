/**
 * Делает слайдер
 * @see  http://api.jqueryui.com/slider/
 *
 * @example
 * // в data-min и data-max задаются минимальное и максимальное значение
 * // в data-step шаг, 
 * // в data-values дефолтные значения "min, max"
 * <div class="slider">
 *      <div class="slider__range" data-min="0" data-max="100" data-step="1" data-values="10, 55"></div>
 * </div>
 */
var Slider = function() {
    var slider = $('.slider'),
        min,
        max,
        step,
        values;

    slider.each(function () {

        var self = $(this),
            range = self.find('.slider__range');

        min = range.data('min');
        max = range.data('max');
        step = range.data('step');
        values = range.data('values').split(', ');

        range.slider({
            range: true,
            min: (min !== null) ? min : null,
            max: (max !== null) ? max : null,
            step: step || 1,
            values: values,
            slide: function(event, ui) {
                var sliderWidth = range.width(),
                    leftHandle = self.find('.ui-slider-handle:nth-child(2)'),
                    rightHandle = self.find('.ui-slider-handle:nth-child(3)'),
                    leftHandleInfo, rightHandleInfo,
                    handlesDistance, infoWidth;

                self.find('.ui-slider-handle').children('span').remove();
                leftHandle.append('<span>' + ui.values[0] + '</span>');
                rightHandle.append('<span>' + ui.values[1] + '</span>');

                leftHandleInfo = leftHandle.children('span');
                rightHandleInfo = rightHandle.children('span');

                handlesDistance = rightHandle.position().left - leftHandle.position().left;
                infoWidth = ( leftHandleInfo.width() + rightHandleInfo.width() ) / 2;

                //console.log( handlesDistance, infoWidth );

                if ( handlesDistance < infoWidth + 20 ) {
                    leftHandleInfo.addClass("is-shifted-left");
                    rightHandleInfo.addClass("is-shifted-right");
                } else {
                    leftHandleInfo.removeClass("is-shifted-left");
                    rightHandleInfo.removeClass("is-shifted-right");
                }
            }
        });

        self.find('.ui-slider-handle:nth-child(2)').append('<span>' + range.slider('values', 0) + '</span>');
        self.find('.ui-slider-handle:nth-child(3)').append('<span>' + range.slider('values', 1) + '</span>');

    });
};

var slider = new Slider();