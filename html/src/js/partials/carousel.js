$(".slick").slick({
        arrows: false,
        dots: true
});

function createMobileCarousel() {
    if ($('.showroom-about-list').length > 0) {
        if ($(window).width() < 640) {
            if (!$('.showroom-about-list').hasClass('slick-initialized')) {
                $('.showroom-about-list').slick({
                    arrows: false,
                    dots: true,
                    speed: 500,
                    infinite: false,
                    slide: '.showroom-about-list-item',
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    responsive: [
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 1
                      }
                    }
                  ]
                });
            }
        } else {
            if ($('.showroom-about-list').hasClass('slick-initialized')) {
                $('.showroom-about-list').slick('unslick');
            }
        }

    }
}


function createGalleryCarousel() {
    if ($('.section-gallery_carousel').length > 0) {
        if ($(window).width() < 1200) {
            if (!$('.section-gallery_carousel').hasClass('slick-initialized')) {
                $('.section-gallery_carousel').slick({
                    arrows: false,
                    dots: true,
                    speed: 500,
                    infinite: false,
                    slide: '.section-gallery__item',
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    responsive: [
                    {
                      breakpoint: 640,
                      settings: {
                        slidesToShow: 1
                      }
                    }
                  ]
                });
            }

        } else {
            if ($('.section-gallery_carousel').hasClass('slick-initialized')) {
                $('.section-gallery_carousel').slick('unslick');
            }
        }

    }
}

$(window).load(function() {
    if ($('.showroom-carousel').length > 0) {
        $('.showroom-carousel').slick({
            arrows: true,
            dots: false,
            speed: 500,
            autoplay: true,
            autopleySpeed: 3000,
            centerPadding: '25%',
            infinite: true,
            centerMode: true,
            slide: '.showroom-carousel__img',
            responsive: [
            {
              breakpoint: 1409,
              settings: {
                centerPadding: '112px',
              }
            },
            {
              breakpoint: 768,
              settings: {
                centerMode: false
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
    }
    createGalleryCarousel();
    createMobileCarousel();
});

var resizeCarousel;
$(window).resize(function(e) {
    clearTimeout(resizeCarousel);
    /**
     * Отрабатывает после завершения события ресайза
     */
    resizeCarousel = setTimeout(function() {
        createGalleryCarousel();
        createMobileCarousel();
    }, 250);
});
