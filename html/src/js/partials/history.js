function runHistorySlider(){
  $('.history__slider').slick({
      infinite: false,
      // touchMove: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: false,
      arrows: false,
      variableWidth: true,
     responsive: [
    {
      breakpoint: 1600,
      settings: {
      slidesToShow: 2
      }
    }
    ]
  }).on( 'DOMMouseScroll mousewheel wheel', function ( event ) {
  var delta = event.originalEvent.deltaY || -event.originalEvent.detail || event.originalEvent.wheelDelta;
    if( delta > 0 ) {
      $('.history__slider').slick('slickNext');
    var slide = $('.history__slider').slick('slickCurrentSlide');
    if (slide <= $('.history__slider').find('.slick-slide').length - $('.history__slider').slick('slickGetOption', 'slidesToShow')) return false;
    } else {
      $('.history__slider').slick('slickPrev');
    var slide = $('.history__slider').slick('slickCurrentSlide');
    if (slide >= 0 ) return false;
    }
  });
}

function resetHistorySlider(){
  if( window.matchMedia('(max-width: 1023px)').matches || $(document).width() < 1023 ) {
      if ($(".history__slider").hasClass('slick-initialized'))
    $(".history__slider").slick("unslick").off('DOMMouseScroll mousewheel wheel');
    }
    else{
      runHistorySlider();
    }
}

$(function () {
//runHistorySlider();

resetHistorySlider();
});

$(window).on( 'resize', resetHistorySlider);