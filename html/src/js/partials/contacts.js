$('.contacts__tabs-nav a').click(function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  $('.contacts__tabs-nav a').removeClass('active');
  $('.contacts__tabs-content > div').hide().removeClass('active-tab');
  $(this).addClass('active');
  $('#'+ id).fadeIn(300, function () {$(this).addClass('active-tab');});
  init();
});

google.maps.event.addDomListener(window, 'load', init);

google.maps.event.addDomListener(window, "resize", init)

function init() {

    if ($('#map-office').length > 0 || $('#map-storage').length > 0) {
        var mapOptionsOffice = {
            zoom: 14,

            center: new google.maps.LatLng(55.796963,37.5376439),

                scrollwheel: false,

                styles: [
                    {
                        "featureType": "administrative.country",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "hue": "#ff0000"
                            }
                        ]
                    }
                ]
            };

        var mapOptionsStorage = {
            zoom: 14,

            center: new google.maps.LatLng(55.970682,37.504638),

            scrollwheel: false,

            styles: [
            {
                "featureType": "administrative.country",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "hue": "#ff0000"
                    }
                ]
            }
        ]
        };

        var mapElement = document.getElementById('map-office');

        var mapElement2 = document.getElementById('map-storage');

        var mapOffice = new google.maps.Map(mapElement, mapOptionsOffice);

        var mapStorage = new google.maps.Map(mapElement2, mapOptionsStorage);

        var image = 'img/balon.png';

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(55.796963,37.5376439),
            map: mapOffice,
            title: 'Офис',
            icon: image
        });

        var marker2 = new google.maps.Marker({
            position: new google.maps.LatLng(55.970682,37.504638),
            map: mapStorage,
            title: 'Складской комплекс',
            icon: image
        });

        google.maps.event.trigger(marker,marker2, "resize");
    }
}
